from dexy.dexy_filter import DexyFilter
import shutil
 
class ForcePhpExtensionFilter(DexyFilter):
    """
    Forces previous filter to output .html extension.
    """
    INPUT_EXTENSIONS = [".*"]
    OUTPUT_EXTENSIONS = [".php"]
    ALIASES = ['forcephp']