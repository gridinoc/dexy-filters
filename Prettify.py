from dexy.dexy_filter import DexyFilter
from bs4 import BeautifulSoup

class PrettifyFilter(DexyFilter):
    ALIASES = ['prettify']

    def process_text(self, input_text):
		soup = BeautifulSoup(input_text, "html5lib", from_encoding="utf-8")
		# return soup.prettify("utf-8", formatter="html")
		return unicode(soup)
		