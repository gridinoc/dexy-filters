# inspired from https://github.com/mintchaos/typogrify/blob/master/typogrify/templatetags/jinja_filters.py
from dexy.dexy_filter import DexyFilter
from typogrify.filters import amp, caps, initial_quotes, smartypants, titlecase, typogrify, widont, TypogrifyError
from functools import wraps
import jinja2
from jinja2.exceptions import TemplateError

def make_safe(f):
    """
    A function wrapper to make typogrify play nice with jinja2's
    unicode support.

    """
    @wraps(f)
    def wrapper(text):
        f.is_safe = True
        out = text
        try:
            out = f(text)
        except TypogrifyError, e:
            raise TemplateError(e.message)
        return jinja2.Markup(out)
    wrapper.is_safe = True
    return wrapper


class TypogrifyFilter(DexyFilter):
    ALIASES = ['typogrify']

    def process_text(self, input_text):
        #t = make_safe(typogrify)
        #return t(input_text)
		return typogrify(input_text)
