from dexy.dexy_filter import DexyFilter
from bs4 import BeautifulSoup
from lxml import etree

class AdapterFilter(DexyFilter):
    ALIASES = ['adapter']

    def process_text(self, input_text):
		soup = BeautifulSoup(input_text, "html5lib", from_encoding="utf-8")
		
		# doc_root = etree.XML(soup.prettify("utf-8", formatter="html"))
		doc_root = etree.XML(unicode(soup))
		
		xslt_root = etree.XML('''\
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
			<xsl:template match="/">
				<h1><xsl:value-of select="//h2" /></h1>
			</xsl:template>
		</xsl:stylesheet>''')
		
		transform = etree.XSLT(xslt_root)
		result = transform(doc_root)
		
		return str(result)