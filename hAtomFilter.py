from dexy.dexy_filter import DexyFilter
from pyquery import PyQuery as pq
from lxml.html.soupparser import fromstring

class hAtomEntryTitleFilter(DexyFilter):
    ALIASES = ['entry-title']

    def process_text(self, input_text):
        d = pq(fromstring(input_text))
        return d('.entry-title').eq(0).text()